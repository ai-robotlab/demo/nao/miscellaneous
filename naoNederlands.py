# -*- encoding: UTF-8 -*-
""" Say `My {Body_part} is touched` when receiving a touch event
"""
ip = "192.168.1.144"
port = 9559

import sys
import time
import argparse
import motion
import almath
import random

from naoqi import ALProxy
from naoqi import ALBroker
from naoqi import ALModule
# Global variable to store the ReactToTouch module instance
ReactToTouch = None
memory = None

class Lenny(ALModule):
    def __init__(self, name):
        ALModule.__init__(self, name)
        self.sr = ALProxy("ALSpeechRecognition", ip, port)
        self.tts = ALProxy("ALTextToSpeech", ip, port)
        while self.sr.SpeechDetected:
            self.tts.say("Yes.")

        #self.listen()
        #while true:
         #   self.listen()

class Test(ALModule):
    def __init__(self, name):
        ALModule.__init__(self, name)

        self.pp = ALProxy("ALRobotPosture", ip, port)
        # self.pp.goToPosture("LyingBelly", 0.667)
        #self.pp.goToPosture("Sit", 0.667)
        self.tts = ALProxy("ALTextToSpeech")
        self.mp = ALProxy("ALMotion", ip, port)
        self.tts.setVolume(1)
        self.tts.setLanguage("Dutch")
        #self.moveHandUp()#("StandZero", 0.667)
        self.tts.say("I wish you a merry christmas!")#, show me your face")
        #self.fd = ALProxy("ALFaceDetection", ip, port)
        #print(self.fd.getLearnedFacesList())
        #if self.fd.f
        #self.fd.learnFace("Yvonne")
        #self.tts.say("Bye!")
        #self.moveHandUp()

    def moveHandUp(self):
        print("I shall move my hand")
        self.mp.wakeUp()
        print("woken up")
        frame        = motion.FRAME_ROBOT
        print("frame established")
        currentTf = self.mp.getTransform("LArm", frame, False)
        print("4")
        # 1
        target1Tf  = almath.Transform(currentTf)
        target1Tf.r2_c4 += 0.0 # y
        target1Tf.r3_c4 += 0.14 # z
        print("5")
        pathList = list(target1Tf.toVector())
        print("6")
        axisMaskList = [almath.AXIS_MASK_VEL]
        print("I'm going to move my hand NOW")
        self.mp.post.transformInterpolations("LArm", frame, pathList, axisMaskList, [2])
        self.tts.say("Here! Take a flyer!")
        print("I am done moving my hand")
        self.mp.openHand("LHand")


class ReactToTouch(ALModule):
    """" A simple module able to react
        to touch events.
    """
    def __init__(self, name):
        ALModule.__init__(self, name)
        # No need for IP and port here because
        # we have our Python broker connected to NAOqi broker

        # Create a proxy to ALTextToSpeech for later use
        self.tts = ALProxy("ALTextToSpeech")
        self.tts.setVolume(1)
        self.tts.setLanguage("Dutch")
        self.pp = ALProxy("ALRobotPosture", ip, port)
        self.mp = ALProxy("ALMotion", ip, port)
        print("led initialisation")
        self.leds = ALProxy("ALLeds", ip, port)
        print(self.leds.listLEDs())
        #names = [
         #   "Face/Led/Red/left/0Deg/Actuator/Value",
          #  "Face/Led/Red/left/90Deg/Actuator/Value",
           # "Face/Led/Red/left/180Deg/Actuator/Value",
            #"Face/Led/Red/left/270Deg/Actuator/Value",]
        #self.leds.createGroup("MyGroup", names)
        print("led success")
        self.pp.goToPosture("Sit", 0.667)
        # Subscribe to TouchChanged event:
        global memory
        memory = ALProxy("ALMemory")
        memory.subscribeToEvent("TouchChanged",
            "ReactToTouch",
            "onTouched")
        

    def onTouched(self, strVarName, value):
        """ This will be called each time a touch
       is detected.

        """

        # Unsubscribe to the event when talking,
        # to avoid repetitions
        memory.unsubscribeToEvent("TouchChanged",
            "ReactToTouch")

        EyeGroup = [
            "LeftFaceLed1",
            "LeftFaceLed2",
            "LeftFaceLed3",
            "LeftFaceLed4",
            "LeftFaceLed5",
            "LeftFaceLed6",
            "LeftFaceLed7",
            "LeftFaceLed8",
            "RightFaceLed1",
            "RightFaceLed2",
            "RightFaceLed3",
            "RightFaceLed4",
            "RightFaceLed5",
            "RightFaceLed6",
            "RightFaceLed7",
            "RightFaceLed8",]

        Sayings = [
            "Hahaha, dat kietelt!",
            "Wat leuk jullie te ontmoeten.",
            "Hier in het robotlab wordt dagelijks hard gewerkt.",
            "Wat een lekker weer vandaag.",
            "Kennen jullie Yvonne al? Zij heeft mij geprogrammeerd!",# And if you don't do what she says, I KILL YOU!",
            "Kunstmatige intelligentie is de toekomst!",
            "Ik word veel gebruikt door masterstudenten voor verschillende projecten.",
            "Hee! Raak me niet aan!",
            "Met de goede software ben ik een heel slimme robot.",
            "Wisten jullie dat robots al beter kunnen schaken dan mensen?",
            "In 2016 heeft een robot de wereldkampioen Goo verslagen!",
            "Waarom is een robot nooit zenuwachtig? Hij heeft stalen zenuwen!"]
        
        print("sayings initialised")
        r = random.randint(0, len(Sayings)-1)
        print("r determined")
        print r
        touched_bodies = []
        print '!'
        print value
        for p in value:
            if p[1]:
                touched_bodies.append(p[0])
        yaw = 0.5 #Arbitrary value within range
        pitch = 0.5
        speed = 0.5
        joints = ["HeadYaw", "HeadPitch"] # Names of the joints (string list)
       # angles = [yaw , pitch] # list of the angles (in radians)
        print touched_bodies
        self.mp.setStiffnesses(joints, 0.8)
        for body in touched_bodies:
            if ('Head/Touch/' in body):
                self.mp.setAngles(joints, [0.0, 0.0], speed)
                self.tts.say("Ik houd heel erg van dansen. Kijk maar!")#. It makes me feel colourful.")#. Say, you look like someone who should study Artificial Intelligence. Let me dance for you!")
                #self.leds.rasta(3.0)
                #self.leds.rotateEyes(100, 0.5, 1.0)
                #self.leds.createGroup("EyeGroup", EyeGroup)
                #print("Eyegroup created")
                #for i in EyeGroup:
                #    self.leds.fadeRGB(i, 1.0, 0.0, 0.0, 1.0)
                #print("faded to red")
                self.dance()
                #self.pp.goToPosture("LyingBelly", 0.667)
                #self.pp.goToPosture("LyingBack", 0.667)
                #self.pp.goToPosture("Sit", 0.667)
                
            elif (body[0] == 'R'):
                yaw = -0.8
                if ('RFoot' in body):
                    pitch = 0.8
                    self.mp.setAngles(joints, [yaw, pitch], speed)
                    print("IMMA DO IT")
                    self.tts.say(Sayings[r])
                    #self.pp.goToPosture("LyingBelly", 0.667)
                    #self.pp.goToPosture("LyingBack", 0.667)
                    #self.pp.goToPosture("Sit", 0.667)
                elif ('RHand' in body):
                    pitch = 0.3
                    self.mp.setAngles(joints, [yaw, pitch], speed)
                    print("IMMA DO IT")
                    self.tts.say(Sayings[r])
                    
            elif (body[0] == 'L'):
                yaw = 0.8
                if ('LFoot' in body):
                    print("let's go")
                    pitch = 0.8
                    self.mp.setAngles(joints, [yaw, pitch], speed)

                    #self.tts.setLanguage("Dutch")

                    #self.tts.say("Hhh")
                    #self.tts.setLanguage("English")
                    self.tts.say(Sayings[r])
                    print("OK")
                elif ('LHand' in body):
                    self.mp.closeHand("LHand")
                    pitch = 0.3
                    self.mp.setAngles(joints, [yaw, pitch], speed)
                    self.tts.say(Sayings[r])

                    #self.moveHandUp()
                    #self.mp.openHand("LHand")
                    #time.sleep(1)
                    #self.mp.closeHand("LHand")
          #  else:
            #    self.say(touched_bodies)

        #self.say(touched_bodies)
        #self.tts.say("Burp")

        self.mp.setAngles(joints, [0.0, 0.0], speed/2)

        # Subscribe again to the event
        memory.subscribeToEvent("TouchChanged",
            "ReactToTouch",
            "onTouched")
        """
    def say(self, bodies):
        if (bodies == []):
            return

        sentence = "My " + bodies[0]

        for b in bodies[1:]:
            sentence = sentence + " and my " + b

        if (len(bodies) > 1):
            sentence = sentence + " are"
        else:
            sentence = sentence + " is"
        sentence = sentence + " touched."

        self.tts.say(sentence)
        """

    def moveHandUp(self):
        print("I shall move my hand")
        self.mp.wakeUp()
        print("woken up")
        frame        = motion.FRAME_ROBOT
        print("frame established")
        currentTf = self.mp.getTransform("LArm", frame, False)
        print("4")
        # 1
        target1Tf  = almath.Transform(currentTf)
        target1Tf.r2_c4 += 0.0 # y
        target1Tf.r3_c4 += 0.14 # z
        print("5")
        pathList = list(target1Tf.toVector())
        print("6")
        axisMaskList = [almath.AXIS_MASK_VEL]
        print("I'm going to move my hand NOW")
        self.mp.post.transformInterpolations("LArm", frame, pathList, axisMaskList, [2])
        self.tts.say("Here! Take a flyer!")
        print("I am done moving my hand")
        self.mp.openHand("LHand")
        self.pp.goToPosture("Sit", 0.667)
       
    def dance(self):
        '''
        Example of a whole body multiple effectors control "LArm", "RArm" and "Torso"
        Warning: Needs a PoseInit before executing
        Whole body balancer must be inactivated at the end of the script
        '''

        # end initialize proxy, begin go to Stand Init
    
        # Wake up robot
        self.mp.wakeUp()
    
        # Send robot to Stand Init
        self.pp.goToPosture("StandInit", 0.667)
    
        # end go to Stand Init, begin initialize whole body
    
        # Enable Whole Body Balancer
        isEnabled  = True
        self.mp.wbEnable(isEnabled)
    
        # Legs are constrained fixed
        stateName  = "Fixed"
        supportLeg = "Legs"
        self.mp.wbFootState(stateName, supportLeg)
    
        # Constraint Balance Motion
        isEnable   = True
        supportLeg = "Legs"
        self.mp.wbEnableBalanceConstraint(isEnable, supportLeg)
    
        # end initialize whole body, define arms motions
    
        useSensorValues = False
    
        # Arms motion
        effectorList = ["LArm", "RArm"]
    
        frame        = motion.FRAME_ROBOT
    
        # pathLArm
        pathLArm = []
        currentTf = self.mp.getTransform("LArm", frame, useSensorValues)
        # 1
        target1Tf  = almath.Transform(currentTf)
        target1Tf.r2_c4 += 0.08 # y
        target1Tf.r3_c4 += 0.14 # z
    
        # 2
        target2Tf  = almath.Transform(currentTf)
        target2Tf.r2_c4 -= 0.05 # y
        target2Tf.r3_c4 -= 0.07 # z
    
        pathLArm.append(list(target1Tf.toVector()))
        pathLArm.append(list(target2Tf.toVector()))
        pathLArm.append(list(target1Tf.toVector()))
        pathLArm.append(list(target2Tf.toVector()))
        pathLArm.append(list(target1Tf.toVector()))
    
        # pathRArm
        pathRArm = []
        currentTf = self.mp.getTransform("RArm", frame, useSensorValues)
        # 1
        target1Tf  = almath.Transform(currentTf)
        target1Tf.r2_c4 += 0.05 # y
        target1Tf.r3_c4 -= 0.07 # z
    
        # 2
        target2Tf  = almath.Transform(currentTf)
        target2Tf.r2_c4 -= 0.08 # y
        target2Tf.r3_c4 += 0.14 # z
    
        pathRArm.append(list(target1Tf.toVector()))
        pathRArm.append(list(target2Tf.toVector()))
        pathRArm.append(list(target1Tf.toVector()))
        pathRArm.append(list(target2Tf.toVector()))
        pathRArm.append(list(target1Tf.toVector()))
        pathRArm.append(list(target2Tf.toVector()))
    
        pathList = [pathLArm, pathRArm]
    
        axisMaskList = [almath.AXIS_MASK_VEL, # for "LArm"
                        almath.AXIS_MASK_VEL] # for "RArm"
    
        coef       = 1.5
        timesList  = [ [coef*(i+1) for i in range(5)],  # for "LArm" in seconds
                       [coef*(i+1) for i in range(6)] ] # for "RArm" in seconds
    
        # called cartesian interpolation
        self.mp.transformInterpolations(effectorList, frame, pathList, axisMaskList, timesList)
    
        # end define arms motions, define torso motion
    
        # Torso Motion
        effectorList = ["Torso", "LArm", "RArm"]
    
        dy = 0.06
        dz = 0.06
    
        # pathTorso
        currentTf = self.mp.getTransform("Torso", frame, useSensorValues)
        # 1
        target1Tf  = almath.Transform(currentTf)
        target1Tf.r2_c4 += dy
        target1Tf.r3_c4 -= dz
    
        # 2
        target2Tf  = almath.Transform(currentTf)
        target2Tf.r2_c4 -= dy
        target2Tf.r3_c4 -= dz
    
        pathTorso = []
        for i in range(3):
            pathTorso.append(list(target1Tf.toVector()))
            pathTorso.append(currentTf)
            pathTorso.append(list(target2Tf.toVector()))
            pathTorso.append(currentTf)
    
        pathLArm = [self.mp.getTransform("LArm", frame, useSensorValues)]
        pathRArm = [self.mp.getTransform("RArm", frame, useSensorValues)]
    
        pathList = [pathTorso, pathLArm, pathRArm]
    
        axisMaskList = [almath.AXIS_MASK_ALL, # for "Torso"
                        almath.AXIS_MASK_VEL, # for "LArm"
                        almath.AXIS_MASK_VEL] # for "RArm"
    
        coef       = 0.5
        timesList  = [
                      [coef*(i+1) for i in range(12)], # for "Torso" in seconds
                      [coef*12],                       # for "LArm" in seconds
                      [coef*12]                        # for "RArm" in seconds
                     ]
    
        self.mp.transformInterpolations(
            effectorList, frame, pathList, axisMaskList, timesList)
    
        # end define torso motion, disable whole body
    
        # Deactivate whole body
        isEnabled    = False
        self.mp.wbEnable(isEnabled)
    
        # Send robot to Pose Init
        self.pp.goToPosture("Sit", 0.667)
    
        # Go to rest position
        self.mp.rest()
    
        # end script

def main(ip, port):
    """ Main entry point
    """
    # We need this broker to be able to construct
    # NAOqi modules and subscribe to other modules
    # The broker must stay alive until the program exists
    myBroker = ALBroker("myBroker",
       "0.0.0.0",   # listen to anyone
       0,           # find a free port and use it
       ip,          # parent broker IP
       port)        # parent broker port

    
    global ReactToTouch
    ReactToTouch = ReactToTouch("ReactToTouch")

    """
    global Test
    Test = Test("Test")
    

    global Lenny
    Lenny = Lenny("Lenny")
    """
    try:
        while True:
            time.sleep(1)
            #val = memory.getData("FaceDetected", 0)
            #print val
    except KeyboardInterrupt:
        print
        print "Interrupted by user, shutting down"
        myBroker.shutdown()
        sys.exit(0)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default="192.168.1.144",
                        help="Robot ip address")
    parser.add_argument("--port", type=int, default=9559,
                        help="Robot port number")
    args = parser.parse_args()
    main(args.ip, args.port)
    
