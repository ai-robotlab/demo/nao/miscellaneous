ip = "192.168.1.102"
port = 9559

import motion
import almath

from naoqi import ALProxy

self.tts = ALProxy("ALTextToSpeech")
self.tts.setVolume(1)
self.tts.setLanguage("Dutch")
self.pp = ALProxy("ALRobotPosture", ip, port)
self.mp = ALProxy("ALMotion", ip, port)

self.pp.goToPosture("LyingBack", 0.667)
"""

def main(ip, port):
    myBroker = ALBroker("myBroker",
       "0.0.0.0",   # listen to anyone
       0,           # find a free port and use it
       ip,          # parent broker IP
       port)        # parent broker port
    self.pp.goToPosture("LyingBack", 0.667)
    myBroker.shutdown()
    sys.exit(0)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default="192.168.1.102",
                        help="Robot ip address")
    parser.add_argument("--port", type=int, default=9559,
                        help="Robot port number")
    args = parser.parse_args()
    main(args.ip, args.port)
"""
