import time
from naoqi import ALProxy


ROBOT_IP = "192.168.1.103"

# Creates a proxy on the speech-recognition module
asr = ALProxy("ALSpeechRecognition", ROBOT_IP, 9559)
mmr = ALProxy("ALMemory", ROBOT_IP, 9559)
pp = ALProxy("ALRobotPosture", ROBOT_IP, 9559)
mp = ALProxy("ALMotion", ROBOT_IP, 9559)


asr.setLanguage("English")
asr.pause(True)
# Example: Adds "yes", "no" and "please" to the vocabulary (without wordspotting)
vocabulary = ["hello", "yes", "no", "please", "dictionary"]
asr.setVocabulary(vocabulary, False)
asr.pause(False)
# Start the speech recognition engine with user Test_ASR
asr.subscribe("MyModule")
print 'Speech recognition engine started'
#if asr.WordRecognized:
 #   print asr.WordRecognized
time.sleep(20)
asr.unsubscribe("MyModule")
#pp.goToPosture("Sit", 0.3)
mp.rest()

